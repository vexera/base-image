FROM node:8.1.1-alpine

ENV NPM_CONFIG_LOGLEVEL warn
RUN apk update && \
    apk upgrade && \
    apk add git python alpine-sdk ffmpeg && \
    npm config set registry http://registry.npmjs.org/

